
tupla = (10,20,30,40)

print(tupla[0])

#Reescribir el valor de una tupla no es válido
#tupla[0] = 50

tupla = 20,30,40,50

print(tupla)

tupla_simple = 50,
print(tupla_simple)

print('-------------TUPLA DE DICCIONARIOS----------')

diccionario_1 = {
    'nombre': 'Juan'
}

diccionario_2 = {
    'nombre': 'María'
}

tupla_diccionarios = (diccionario_1, diccionario_2)
print('Valor inicial de la tupla')
print(tupla_diccionarios)

diccionario_1['apellido'] = 'Delgado'
print('Nuevo valor')
print(tupla_diccionarios)


print('-------------DESEMPAQUETAR TUPLA-----------')
tupla_numeros = 10,20,30,40,50
print(tupla_numeros)
#Asignación múltiple para desempaquetar una tupla
n1,n2,n3,n4,n5 = tupla_numeros
print(n1)


def numeros():
    n1 = 10
    n2 = 20
    return n1, n2

def nombres()->tuple:
    pass